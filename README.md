# SampleServer

A project meant to be an example of a Hello World server.

## Run
- Start iex with `iex -S mix`
- Run the server with `Plug.Adapters.Cowboy2.http SampleServer, []`
- Go to localhost:4000

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/sample_server](https://hexdocs.pm/sample_server).

