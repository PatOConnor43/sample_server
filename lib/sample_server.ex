defmodule SampleServer do
  import Plug.Conn
  @moduledoc """
  Documentation for SampleServer.
  """

  @doc """
  Hello world.

  ## Examples

      iex> SampleServer.hello
      :world

  """

    def init(options) do
      # initialize options

      options
    end

    def call(conn, _opts) do
      conn
      |> put_resp_content_type("text/plain")
      |> send_resp(200, "Hello world")
    end
end
